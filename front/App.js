import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import Header from './components/Header'
import Section from './components/Section'
import {DataProvider} from './components/Context'


class App extends React.Component{
  render(){
    return(

      <DataProvider>
    {/*    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />*/}
    {/*<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js">*/}
    {/*    </script>*/}
    {/*     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>*/}
    {/*      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>*/}

        <div className="app">
          <Router>
            <Header />
            <Section />


          </Router>
        </div>
      </DataProvider>
    );
  }
}

export default App;