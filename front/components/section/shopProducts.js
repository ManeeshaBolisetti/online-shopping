import React, { Component } from 'react'
import axios from 'axios';

export const DataContext = React.createContext();

class ShopProducts extends React.Component {

    state = {
        details: [],
        cart: [],
        total: 0
    }



    addCart = (id) => {
        const {details, cart} = this.state;
        const check = cart.every(item => {
            return item.id !== id
        })
        if (check) {
            const data = details.filter(product => {
                return product.id === id
            })
            this.setState({cart: [...cart, ...data]})
        } else {
            alert("The product has been added to cart.")
        }
    };
    reduction = id => {
        const {cart} = this.state;
        cart.forEach(item => {
            if (item.id === id) {
                item.Count === 1 ? item.Count = 1 : item.Count -= 1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };
    increase = id => {
        const {cart} = this.state;
        cart.forEach(item => {
            if (item.id === id) {
                item.Count += 1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };
    removeProduct = id => {
        if (window.confirm("Do you want to delete this product?")) {
            const {cart} = this.state;
            cart.forEach((item, index) => {
                if (item.id === id) {
                    cart.splice(index, 1)
                }
            })
            this.setState({cart: cart});
            this.getTotal();
        }

    };

    getTotal = () => {
        const {cart} = this.state;
        const res = cart.reduce((prev, item) => {
            return prev + (item.Price * item.Count);
        }, 0)
        this.setState({total: res})
    };

    componentDidUpdate() {
        localStorage.setItem('dataCart', JSON.stringify(this.state.cart))
        localStorage.setItem('dataTotal', JSON.stringify(this.state.total))
    };
 componentDidMount()

    {

        let data;

        axios.get('http://127.0.0.1:8000/backend/prod/')
            .then(res => {
                data = res.data;
                this.setState({
                    details: data
                });
            })
            .catch(err => {
            })
        const dataCart = JSON.parse(localStorage.getItem('dataCart'));
        if(dataCart !== null){
            this.setState({cart: dataCart});
        }
        const dataTotal = JSON.parse(localStorage.getItem('dataTotal'));
        if(dataTotal !== null){
            this.setState({total: dataTotal});
        }
    };
    render() {

        const {products, cart, total} = this.state;
        const {addCart, reduction, increase, removeProduct, getTotal} = this;
        return (
            <DataContext.Provider
                value={{products, addCart, cart, reduction, increase, removeProduct, total, getTotal}}>
                {this.props.children}
            </DataContext.Provider>
        )
    }
}
export default shopProducts;