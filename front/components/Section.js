import React, { Component } from 'react'
import Products from './section/Products'
import Details from './section/Details'
import {Route} from "react-router-dom"
import Cart from './section/Cart'
import Payment from './section/Payment'
import Home from "./Home";
import Example from "./section/Example";

import Exp from "./section/Exp";
import Login from './section/Login';


export class Section extends Component {
    render() {
        return (
            <section>
                    <Route path="/" component={Products} exact />
                    <Route path="/product" component={Products} exact  />
                    <Route path="/example" component={Example} exact  />
                    <Route path="/exp" component={Exp} exact  />
                    <Route path="/shop" component={Home} exact  />
                    <Route path="/login" component={Login} exact  />
                    <Route path="/product/:id" component={Details} exact />
                    <Route path="/cart" component={Cart}  exact/>
                    <Route path="/payment" component={Payment} exact />

            {/*<Home />*/}
            {/*    <Example />*/}
            {/*<Exp />*/}
            </section>
        )
    }
}

export default Section
