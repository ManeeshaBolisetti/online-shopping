from django.contrib import admin

# Register your models here.
from backend.models import product, Orders, OrderItems


admin.site.register(product)
admin.site.register(Orders)
admin.site.register(OrderItems)