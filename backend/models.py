from django.contrib.auth.models import User
from django.db import models



class product(models.Model):
    Title = models.CharField(max_length=30)
    Description = models.CharField(max_length=150)
    # ImageLink = models.FileField(upload_to='photos/', blank=True, null=True)
    Plink=models.CharField(max_length=1000)
    Price = models.FloatField()
    Count = models.IntegerField(default=1)
    CreatedAt = models.DateField(auto_now_add=True, null=True)
    UpdatedAt = models.DateField(auto_now=True, null=True)

    def __str__(self):
        return f"{self.id}{self.Title}"


#
# class Account(models.Model):
#     email=models.EmailField(max_length=225)
#     username=models.CharField(max_length=50)
#     password1=models.CharField(max_length=50)
#     password2=models.CharField(max_length=50)
#
#     def __str__(self):
#         return f"{self.username}{self.email}"


class Orders(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    Total = models.IntegerField(default=0)
    CreatedAt = models.DateField(auto_now_add=True, null=True)
    UpdatedAt = models.DateField(auto_now=True, null=True)
    STATUS = (
        (1, ('New')),
        (2, ('Paid')),

    )
    # [...]
    Status = models.PositiveSmallIntegerField(
        choices=STATUS,
        default=1,
    )
    PAYMENT = (
        (1, ('CashOnDelivery')),
        (2, ('Paytm')),
        (3, ('Card')),

    )
    # [...]
    PaymentModel = models.PositiveSmallIntegerField(
        choices=PAYMENT,
        default=1,
    )

    def __str__(self):
        return f'{self.id}{self.user}{self.Total}{self.CreatedAt}{self.UpdatedAt}{self.Status}{self.PaymentModel}'


class OrderItems(models.Model):
    OrderId = models.ForeignKey(Orders, on_delete=models.CASCADE)
    ProductId = models.ForeignKey(product, on_delete=models.CASCADE)
    Quantity=models.IntegerField(default=1)
    Price=models.FloatField(default=0)


    def __str__(self):
        return f'{self.id}{self.OrderId}{self.ProductId}{self.Quantity}{self.Price}'

